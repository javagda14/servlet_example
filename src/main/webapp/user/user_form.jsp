<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 10/3/18
  Time: 8:08 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User form</title>
</head>
<body>
<h2>User Form:</h2>
<%-- http://localhost:8080/user/add --%>
<span style="color: red; ">
    <c:if test="${not empty error_message}">
        <c:out value="${error_message}"/>
    </c:if>
</span>

<c:choose>
    <c:when test="${not empty user_to_modify}">
        <form action="/user/modify" method="post">
            <input type="hidden" name="id" value="<c:out value="${user_to_modify.id}"/>">
            <div>
                <label for="username">Username:</label>
                <input id="username" name="username" type="text" value="<c:out value="${user_to_modify.login}"/>">
            </div>
            <div>
                <label for="password">Password:</label>
                <input id="password" name="password" type="password"
                       value="<c:out value="${user_to_modify.password}"/>">
            </div>
            <div>
                <label for="password-confirm">Password confirm:</label>
                <input id="password-confirm" name="password-confirm" type="password"
                       value="<c:out value="${user_to_modify.password}"/>">
            </div>
            <input type="submit" value="Modify">
        </form>
    </c:when>
    <c:otherwise>
        <form action="/user/add" method="post">
            <div>
                <label for="username">Username:</label>
                <input id="username" name="username" type="text">
            </div>
            <div>
                <label for="password">Password:</label>
                <input id="password" name="password" type="password">
            </div>
            <div>
                <label for="password-confirm">Password confirm:</label>
                <input id="password-confirm" name="password-confirm" type="password">
            </div>
            <input type="submit" value="Register">
        </form>
    </c:otherwise>
</c:choose>
</body>
</html>
