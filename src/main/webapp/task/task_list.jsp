<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 10/5/18
  Time: 5:37 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Task list</title>
</head>
<body>
<h2>Task List:</h2>


<table>
    <thead>
    <th>Id</th>
    <th>Title</th>
    <th>Content</th>
    <th>Done</th>
    <th>Action remove</th>
    <th>Action modify</th>
    </thead>
    <tbody>
    <c:forEach items="${taskList}" var="task">
        <tr>
            <td>
                <c:out value="${task.id}"/>
            </td>
            <td>
                <c:out value="${task.title}"/>
            </td>
            <td>
                <c:out value="${task.content}"/>
            </td>
            <td>
                <c:out value="${task.done}"/>
            </td>
            <td>
                <a href="/task/remove?id=<c:out value="${task.id}"/>">Remove</a>
            </td>
            <td>
                <a href="/task/modify?id=<c:out value="${task.id}"/>">Modify</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>

</body>
</html>
