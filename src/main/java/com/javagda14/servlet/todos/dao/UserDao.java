package com.javagda14.servlet.todos.dao;

import com.javagda14.servlet.todos.model.AppUser;
import com.javagda14.servlet.todos.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionException;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.xml.registry.infomodel.User;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserDao {
    public void add(AppUser newUser) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            session.save(newUser);

            transaction.commit();
        } catch (SessionException sessionException) {
            transaction.rollback();
            System.out.println("Error");
        }
    }

    public List<AppUser> getAllUsers() {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            List<AppUser> list = session.createQuery("from AppUser", AppUser.class).list();

            return list;
        } catch (SessionException sessionException) {
            transaction.rollback();
            System.out.println("Error");
        }
        return new ArrayList<>();
    }

    public void removeUserWithId(int id) {
        Optional<AppUser> userOpt = findUserWithId(id);

        if (userOpt.isPresent()) {
            AppUser user = userOpt.get();

            Transaction transaction = null;
            try (Session session = HibernateUtil.getSessionFactory().openSession()) {
                transaction = session.beginTransaction();
                session.delete(user);

                transaction.commit();
            } catch (SessionException sessionException) {
                transaction.rollback();
                System.out.println("Error");
            }
        }
    }

    public Optional<AppUser> findUserWithId(int userId) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            Query<AppUser> query = session.createQuery("from AppUser where id= :id", AppUser.class);
            query.setParameter("id", userId);

            AppUser result = query.uniqueResult();

            return Optional.of(result);
        } catch (SessionException sessionException) {
            transaction.rollback();
            System.out.println("Error");
        }
        return Optional.empty();
    }

    public void updateUser(AppUser userToModify) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
//            Query<AppUser> userQuery = session.createQuery("update AppUser set login=:login, password=:password where id= :id", AppUser.class);
//            userQuery.setParameter("login", userToModify.getLogin());
//            userQuery.setParameter("password", userToModify.getPassword());
//            userQuery.setParameter("id", userToModify.getId());
            session.saveOrUpdate(userToModify);

//            System.out.println("Changed : " + userQuery.executeUpdate() + " records");
            transaction.commit();
        } catch (SessionException sessionException) {
            transaction.rollback();
            System.out.println("Error");
        }
    }
}
