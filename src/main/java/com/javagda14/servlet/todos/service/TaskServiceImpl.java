package com.javagda14.servlet.todos.service;

import com.javagda14.servlet.todos.model.TodoTask;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TaskServiceImpl implements TaskService {

    private List<TodoTask> taskList = new ArrayList<>();

    @Override
    public void addTask(TodoTask newTask) {
        taskList.add(newTask);
    }

    @Override
    public List<TodoTask> getTaskList() {
        return taskList;
    }

    @Override
    public void removeTaskWithId(int id) {
        taskList.removeIf(task -> task.getId() == id);
    }

    @Override
    public Optional<TodoTask> getTaskWithId(int taskId) {
        return taskList.stream().filter(task -> task.getId() == taskId).findFirst();
    }
}
