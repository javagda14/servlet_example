package com.javagda14.servlet.todos.service;

import com.javagda14.servlet.todos.dao.UserDao;
import com.javagda14.servlet.todos.model.AppUser;

import javax.enterprise.inject.Model;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserServiceImpl implements UserService {

    private UserDao userDao;

    public UserServiceImpl() {
        userDao = new UserDao();
    }

    @Override
    public void addUser(AppUser newUser) {
        userDao.add(newUser);
    }

    @Override
    public List<AppUser> getUserList() {
        return userDao.getAllUsers();
    }

    @Override
    public void removeUserWithId(int id) {
        userDao.removeUserWithId(id);
    }

    @Override
    public Optional<AppUser> getUserWithId(int userId) {
        return userDao.findUserWithId(userId);
    }

    @Override
    public void modify(AppUser userToModify) {
        userDao.updateUser(userToModify);
    }
}
