package com.javagda14.servlet.todos.controller;

import com.javagda14.servlet.todos.service.DependencyManager;
import com.javagda14.servlet.todos.service.TaskService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/task/list")
public class TaskListController extends HttpServlet {

    private TaskService taskService;

    public TaskListController() {
        taskService = DependencyManager.getInstance().getBean(TaskService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("taskList", taskService.getTaskList());

        req.getRequestDispatcher("/task/task_list.jsp").forward(req, resp);
    }
}
