package com.javagda14.servlet.todos.controller;

import com.javagda14.servlet.todos.model.AppUser;
//import com.javagda14.servlet.todos.service.DependencyManager;
import com.javagda14.servlet.todos.service.DependencyManager;
import com.javagda14.servlet.todos.service.UserService;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/user/add")
public class UserAddController extends HttpServlet {

    @Inject
    private UserService userService;

    public UserAddController() {
        userService = DependencyManager.getInstance().getBean(UserService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/user/user_form.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String password_confirm = req.getParameter("password-confirm");

        if (username.isEmpty() || password.isEmpty() || !password.equals(password_confirm)) {
            req.setAttribute("error_message", "Incorrect username or password!");

            req.getRequestDispatcher("/user/user_form.jsp").forward(req, resp);
            return;
        }

        // stworzyć user'a i ustawić mu wszystkie parametry
        AppUser newUser = new AppUser(username, password);
        userService.addUser(newUser);

        resp.sendRedirect(req.getContextPath() + "/user/list");
    }
}
